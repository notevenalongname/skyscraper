# `skyscraper`

**`skyscraper`** is a customizable scraper for Google Spreadsheets, written in
Python 3.

#### Dependencies

- Python 3
- `lxml`
- `cssselect`
- `openpyxl` to import to Excel (`.xlsx`)

#### Usage

First, get the ID of your target spreadsheet. You can find it in the URL:
`https://docs.google.com/spreadsheets/d/{token}/...`

```python
import skyscraper

spreadsheet = skyscraper.fetch("Your spreadsheet token here")

print(spreadsheet)

# Grab a sheet by name, then access it
print(spreadsheet["Sheet 1"]["A":14])   # The value of cell A14
print(spreadsheet["Sheet 1"]["A":])     # An iterator over all cells in column A
print(spreadsheet["Sheet 1"][:14])      # An iterator over all cells in row 14
print(spreadsheet["Sheet 1"][:])        # An iterator over all cells of Sheet 1

```
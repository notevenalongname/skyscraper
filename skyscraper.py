################################################################################
##                                 skyscraper                                 ##
##                                                                            ##
##            A customizable scraping tool for Google Spreadsheets            ##
################################################################################


import lxml.html
import urllib.request


# # # # # # #
# Constants #
# # # # # # #

LINK_TEMPLATE = "https://docs.google.com/spreadsheets/d/{}/htmlview"


# # # # # # # # # #
# Data structures #
# # # # # # # # # #

class spreadsheet:
    """
    A spreadsheet is a collection of sheets. You can iterate over the sheets as
    if they were contained in a dictionary, but they are accessible both by ID
    and sheet title.
    """
    def __init__(self):
        self._sheets = {}
        self._name_id_map = {}
        
    def __iter__(self):
        return iter(self._name_id_map)
        
    def __getitem__(self, item):
        if isinstance(item, str):
            return self._sheets[self._name_id_map[item]]
        elif isinstance(item, int):
            return self._sheets[item]
        else:
            raise KeyError("Unknown sheet: " + repr(item))
    
    def _add_sheet(self, sheet_id, sheet_name):
        """
        Used internally to add newly discovered sheets to the document.
        """
        self._sheets[sheet_id] = sheet(sheet_id, sheet_name)
        self._name_id_map[sheet_name] = sheet_id
        
    def to_excel(self, filename):
        """
        Exports the data to an .xlsx file using the openpyxl library
        """
        import openpyxl
        workbook = openpyxl.Workbook()
        
        index = 0
        for sheet_name in self:
            current = self[sheet_name]
            if index == 0:
                ws = workbook.active
                ws.title = sheet_name
            else:
                ws = workbook.create_sheet(title=sheet_name)
            for row in range(len(current._data)):
                for col in range(len(current._data[row])):
                    value = current._data[row][col]
                    if isinstance(value, merged_cell):
                        ws.merge_cells(start_row=value.row + 1, start_column=value.column + 1, end_row=row + 1, end_column=col + 1)
                    else:
                        ws.cell(column=col + 1, row=row + 1, value=value)
            index += 1
        workbook.save(filename=filename)

class sheet:
    """
    A sheet contains the actual data within a spreadsheet. Each sheet has a name
    and an ID, and contains a table storing the data.
    """
    def __init__(self, sheet_id, sheet_name):
        self.sheet_id = sheet_id
        self.sheet_name = sheet_name
        self._data = []
    
    def __getitem__(self, item):
        if not isinstance(item, slice):
            raise IndexError("Cannot return item at " + repr(item))
        column = _column_index_from_name(item.start.upper())
        row = item.stop - 1
        if row == None and column == None:
            # Return an iterator over the entire sheet
            raise KeyError("Not supported yet: sheet iteration")
        elif row == None:
            # Return an iterator over the entire column
            raise KeyError("Not supported yet: column iteration")
        elif column == None:
            # Return an iterator over the entire row
            raise KeyError("Not supported yet: row iteration")
        else:
            # Return the value at the target cell
            return self._data[row][column]
    
    def __setitem__(self, item, value):
        """
        Sets a value in the sheet. If you specify the third slice parameter
        (usually the "step size"), and it evaluates to true, ignore the protection
        of merged cells. This will not allow you to add new cells to the spreadsheet.
        """
        if not isinstance(item, slice):
            raise IndexError("Cannot set item at " + repr(item))
        
        col = _column_index_from_name(item.start.upper())
        row = item.stop - 1
        ignore_merged = bool(item.step)
        
        if row == None and column == None:
            # Set the entire sheet
            for row in range(len(self._data)):
                for col in range(len(self._data[row])):
                    if ignore_merged or not isinstance(self._data[row][col], merged_cell):
                        self._data[row][col] = value
        elif row == None:
            # Set the values in an entire column
            for row in range(len(self._data)):
                if ignore_merged or not isinstance(self._data[row][col], merged_cell):
                    try:
                        self._data[row][col] = value
                    except:
                        # The column "skipped" this row, add it back in
                        while len(self._data[row]) < col:
                            self._data[row].append("")
                        self._data[row].append(value)
        elif column == None:
            # Set the values in an entire row
            for col in range(len(self._data[row])):
                if ignore_merged or not isinstance(self._data[row][col], merged_cell):
                    self._data[row][col] = value
        else:
            # Set the value at the target cell (will not create the cell)
            self._data[row][column] = value
    
class merged_cell:
    """
    Marks a cell as merged into another cell. You can query the "owner" of this
    cell via the 'row' and 'column' members.
    """
    def __init__(self, target_row, target_column):
        self.row = target_row
        self.column = target_column


# # # # # # #
# Functions #
# # # # # # #

def fetch(token, log=False):
    """
    Downloads and parses the spreadsheet with the given token. The token can be
    extracted from the URL: 
                https://docs.google.com/spreadsheets/d/{token}/...
    """
    with urllib.request.urlopen(LINK_TEMPLATE.format(token)) as response:
        # Read HTML
        html_bytes = response.read()
        # Convert to a parseable string (from UTF8 bytes)
        html = str(html_bytes, encoding='utf-8')
        # Parse and return
        return parse(html, log)

def parse(html, log=False):
    """
    Parses the internal structure of an exported spreadsheet.
    """
    root = lxml.html.document_fromstring(html)
    document = spreadsheet()
    
    # Grab sheet names and IDs. Those are listed in the only <ul> tag within the
    # document. It contains an <li> tag for each sheet, whose 'id' attribute 
    # allows us to determine the sheet ID, and whose text content is the sheet
    # name.
    for ul in root.cssselect('ul'):
        for li in ul.cssselect('li'):
            # The tag text is the sheet title
            sheet_title = li.text_content()
            
            # The actual tag id looks like 'sheet-button-ID', so we remove the
            # first two parts
            sheet_id = int(li.get('id').split("-")[-1])
            
            # Add the sheet
            document._add_sheet(sheet_id, sheet_title)
            
            if log:
                print("Found sheet '{}' (ID {})".format(sheet_title, sheet_id))

    # Grab tables. One <table> tag per sheet exists.
    for table in root.cssselect('table'):
        sheet_id = int(table.getparent().getparent().get('id'))
        
        if log:
            print("Reading table for sheet ID {}".format(sheet_id))
        
        # Iterate over every cell in the table
        merged_cells = {}
        
        row_index = 0
        for row in table.cssselect('tr'):
            if row_index not in merged_cells:
                merged_cells[row_index] = {}
            document[sheet_id]._data.append([])
            
            col_index = 0
            iterator = iter(row.cssselect('td'))
            while True:
                if col_index not in merged_cells[row_index]:
                    try:
                        cell = next(iterator)
                    except StopIteration:
                        break
                    # Get rowspan and colspan
                    rowspan_value = cell.get('rowspan')
                    colspan_value = cell.get('colspan')
                    
                    rowspan = 1 if rowspan_value == None else int(rowspan_value)
                    colspan = 1 if colspan_value == None else int(colspan_value)
                    
                    # Block off the merged cells
                    for row_offset in range(rowspan):
                        for col_offset in range(colspan):
                            if row_offset == 0 and col_offset == 0:
                                continue
                            if row_index + row_offset not in merged_cells:
                                merged_cells[row_index + row_offset] = {col_index + col_offset: (row_index, col_index)}
                            else:
                                merged_cells[row_index + row_offset][col_index + col_offset] = (row_index, col_index)
                    
                    document[sheet_id]._data[-1].append(cell.text_content())
                else:
                    document[sheet_id]._data[-1].append(merged_cell(*merged_cells[row_index][col_index]))
                col_index += 1
            del merged_cells[row_index]
            row_index += 1
    return document
